<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::where('email','chintangohil2000@gmail.com')->get()->first();
        if(!$user){
            \App\User::create([
                'name'=>'Chintan Gohil',
                'email'=>'chintangohil2000@gmail.com',
                'password'=>\Illuminate\Support\Facades\Hash::make('chintan123'),
                'role'=>'admin'
            ]);
        } else {
            $user->update(['role'=>'admin']);
        }

        \App\User::create([
            'name'=>'Jash Doshi',
            'email'=>'jash@orbitero.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('jash123'),
        ]);
        \App\User::create([
            'name'=>'raj panchal',
            'email'=>'raj@orbitero.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('raj123'),
        ]);
        \App\User::create([
            'name'=>'Ashay Gogri',
            'email'=>'ashay@orbitero.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('ashay123'),
        ]);
        \App\User::create([
            'name'=>'Yash Thakkar',
            'email'=>'yashthakkar2700@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
        ]);
    }
}