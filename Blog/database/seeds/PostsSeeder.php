<?php

use Illuminate\Database\Seeder;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = \App\Category::create(['name'=>'News']);
        $categoryDesign = \App\Category::create(['name'=>'Design']);
        $categoryTechnology = \App\Category::create(['name'=>'Technology']);
        $categoryEngineering = \App\Category::create(['name'=>'Engineering']);

        $tagCustomers = \App\Tags::create(['name'=>'customers']);
        $tagDesign = \App\Tags::create(['name'=>'design']);
        $tagCoding = \App\Tags::create(['name'=>'coding']);
        $tagLaravel = \App\Tags::create(['name'=>'laravel']);

        $post1 = \App\Post::create([
            'title'=>'We relocated our office to HOME',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10, 18)),
            'content'=>Faker\Factory::create()->sentence(rand(3, 7), true),
            'image'=>'posts/1.jpg',
            'category_id'=>$categoryDesign->id,
            'user_id'=>2,
            'published_at'=>\Carbon\Carbon::now()->format('Y-m-d')
        ]);

        $post2 = \App\Post::create([
            'title'=>'How java is easy',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10, 18)),
            'content'=>Faker\Factory::create()->sentence(rand(3, 7), true),
            'image'=>'posts/2.jpg',
            'category_id'=>$categoryDesign->id,
            'user_id'=>3,
            'published_at'=>\Carbon\Carbon::now()->format('Y-m-d')
        ]);

        $post3 = \App\Post::create([
            'title'=>'Lets start Laravel',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10, 18)),
            'content'=>Faker\Factory::create()->sentence(rand(3, 7), true),
            'image'=>'posts/3.jpg',
            'category_id'=>$categoryDesign->id,
            'user_id'=>4,
            'published_at'=>\Carbon\Carbon::now()->format('Y-m-d')
        ]);

        $post4 = \App\Post::create([
            'title'=>'Begin with OOP Concepts',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10, 18)),
            'content'=>Faker\Factory::create()->sentence(rand(3, 7), true),
            'image'=>'posts/4.jpg',
            'category_id'=>$categoryDesign->id,
            'user_id'=>5,
            'published_at'=>\Carbon\Carbon::now()->format('Y-m-d')
        ]);

        $post5 = \App\Post::create([
            'title'=>'Going with markups',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10, 18)),
            'content'=>Faker\Factory::create()->sentence(rand(3, 7), true),
            'image'=>'posts/5.jpg',
            'category_id'=>$categoryDesign->id,
            'user_id'=>6,
            'published_at'=>\Carbon\Carbon::now()->format('Y-m-d')
        ]);

        $post1->tags()->attach([$tagCoding->id, $tagLaravel->id]);
        $post2->tags()->attach([$tagCustomers->id, $tagCoding->id, $tagLaravel->id, $tagDesign->id]);
        $post3->tags()->attach([$tagCoding->id, $tagLaravel->id]);
        $post4->tags()->attach([$tagCoding->id, $tagLaravel->id]);
        $post5->tags()->attach([$tagCoding->id, $tagLaravel->id, $tagDesign->id, $tagCustomers->id]);

    }
}
