
@if ($paginator->hasPages())
    <div class="row mt25 animated" data-animation="fadeInUp" data-animation-delay="100">
         @if ($paginator->onFirstPage())
            <div class="col-md-6">
                <a href="javascript:;" class="button button-sm button-pasific pull-left hover-skew-backward">
                    Old Entries
                </a>
            </div>
        @else
            <div class="col-md-6">
                <a href="{{$paginator->previousPageUrl()}}" class="button button-sm button-pasific pull-left hover-skew-forward">Old Entries</a>
            </div>
        @endif

        @if ($paginator->hasMorePages())
            <div class="col-md-6">
                <a href="{{$paginator->nextPageUrl()}}" class="button button-sm button-pasific pull-right hover-skew-forward">New Entries</a>
            </div>
        @else
            <div class="col-md-6">
                <a href="javascript:;" class="button button-sm button-pasific pull-right hover-skew-forward">New Entries</a>
            </div>
        @endif
    </div>
@endif


<!-- @if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true">
                    <span class="page-link">@lang('pagination.previous')</span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a>
                </li>
            @endif

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true">
                    <span class="page-link">@lang('pagination.next')</span>
                </li>
            @endif
        </ul>
    </nav>
@endif -->
